// This document contains all the endpoints for our application (also http methods)
/*

	const taskController = require("../controllers/taskController.js");
	const express = require("express");
	const router = express.Router();


	// were not submitting data in get 
	router.get("/viewTasks", (req, res) =>{
		// Invokes the "getAllTask" function from the "taskController.js" file and sends the result back to client/Postman
										// returns result from our controller
		taskController.getAllTask().then(result =>
				res.send(result))

	})

	// in post it is required that there is input. thats why we request for the document which is req.body
	router.post("/addNewTask", (req, res) => {
		taskController.createTask(req.body).then(result => res.send(
			result));
	})

	module.exports=router;
*/

const express = require("express");
const router = express.Router();
const taskController = require("../controllers/taskController.js");

router.get("/viewTasks", (req, res) => {
	// Invokes the "getAllTasks" function from the "taskController.js" file and sends the result back to the client/Postman
							//returns result from our controller
	taskController.getAllTask().then(result => res.send(result));
})

router.post("/addNewTask" , (req, res) => {
	taskController.createTask(req.body).then(result => res.send(result));
})

							//:id will serve as our wildcard
router.delete("/deleteTask/:id", (req, res) => {
	taskController.deleteTask(req.params.id).then(result => res.send(
		result));
})

router.put("/updateTask/:id", (req, res) => {
	/*
		req.params.id - will be the basis of what document we will update
		req.body - the new documents/contents
	*/
	taskController.updateTask(req.params.id, req.body).then(result =>
		res.send(result));
})

// Search a specific task via ID
router.get("/search/:id", (req, res) => {
	taskController.getTaskId(req.params.id).then(result =>
		res.send(result));
})

// Search and Update task status
router.put("/:id/complete", (req, res) => {
	taskController.updateStatus(req.params.id, req.body).then(result =>
		res.send(result));
})

module.exports = router;

